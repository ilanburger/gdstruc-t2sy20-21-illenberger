package com.gdstruc.quiz3;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	/*Instructions
    Create a matchmaking algorithm for players.

    Mechanics:

    Every turn, x players will queue for matchmaking (x = rand () 1 to 7). Pressing enter ends the turn.
    A game can be started when at least 5 players are in the queue.
    When a game starts, pop the first 5 players from the queue.
    The program terminates when 10 games have been successfully made.*/

       PlayerQueue matchQueue = new PlayerQueue(7);
       int gameCount = 0;

       while (gameCount < 10) {
           //while gamecount is still less than 10, keep queueing
           System.out.println("==================================");
           enlistPlayers(matchQueue); //get some players
           System.out.println("==================================");

           //check if queue full and ready.
           if(matchQueue.size() >= 5){
            System.out.println("==================================");
            System.out.println("Queue full. Match commencing now...");
            System.out.println("==================================");

            //start game
            startGame(matchQueue);
            gameCount++;
            System.out.println("GAME COUNT:"+ gameCount);
            }
           else {
               System.out.println( matchQueue.size() + "/5 players. Waiting for other players to connect...");
            }
           System.out.println("==================================");
           System.out.println("Press Any Key To Continue...");
           new Scanner(System.in).nextLine();
       }
       System.out.println("Max number of games have been reached, ending program now.");
    }

    public static PlayerQueue enlistPlayers(PlayerQueue playerQueue){
        String playerNames[] = {"Lukas", "Oliver", "Neun", "Enri", "Ebel", "Emma",
                "Amalia", "D-039", "D-029", "S-009", "D-021", "D-040"};
        int nameIndex, playerCount, level;
        Random r = new Random();
        Player enlist = null;

        //check if theres remaining players and decrease random bounds accordingly, prevents overloading
        if (playerQueue.size() != 0){
            playerCount = 1 + r.nextInt(7-playerQueue.size());
        }
        else {
            playerCount = 1 + r.nextInt(7); //determines how many players are logged in.
        }
        System.out.println(playerCount + " players have been found...");
        for (int i = 0; i < playerCount; i++){
            nameIndex = r.nextInt(10);
            level = 1 + r.nextInt(10);
            enlist = new Player(playerNames[nameIndex], level);
            playerQueue.add(enlist);
        }
        playerQueue.printQueue();
        System.out.println( "Currently at "+ playerQueue.size() + "/5 players.");
        return playerQueue;
    }

    public static void startGame(PlayerQueue playerQueue)
    {
            for (int i = 0; i < 5; i++) {
                System.out.println("Logging " + playerQueue.peek() + " into match.");
                playerQueue.remove();
            }

            //check if there are extra players, move them to a new queue
            if (playerQueue.size() != 0) {
                System.out.println("==================================");
                System.out.println(playerQueue.size() + " other players remaining. Moving to a new queue...");
            }

    }
}
