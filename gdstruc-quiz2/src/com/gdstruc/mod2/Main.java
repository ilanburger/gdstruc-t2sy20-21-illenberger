package com.gdstruc.mod2;

public class Main {

    public static void main(String[] args) {
	Player asuna = new Player(1,"Asuna",100);
        Player lethalBacon= new Player(2,"LethalBacon",205);
        Player hpDeskjet = new Player (3,"HPDeskjet",34);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet); //this will come in first becos its added to the front last

        System.out.println("Before deleting: ");
        playerLinkedList.printList();

        //playerLinkedList.removeFront();
        System.out.println("After deleting: ");
        playerLinkedList.printList();

        System.out.println(playerLinkedList.contains(hpDeskjet));
        System.out.println(playerLinkedList.indexOf(asuna));

    }
}
