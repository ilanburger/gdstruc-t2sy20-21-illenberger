package com.gdstruc.mod2;

import java.util.Objects;

public class PlayerNode {
    private Player player;
    private PlayerNode nextPlayer;
    private PlayerNode prevPlayer;

    public PlayerNode(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(PlayerNode nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public PlayerNode getPrevPlayer() {
        return prevPlayer;
    }

    public void setPrevPlayer(PlayerNode prevPlayer) {
        this.prevPlayer = prevPlayer;
    }

    //this is needed if u want to access & print the contents


   /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerNode playerNode = (PlayerNode) o;
        return player.getId() == playerNode.player.getId() && player.getLevel() == playerNode.player.getLevel() && Objects.equals(player.getName(), playerNode.player.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(player.getId(),player.getName(), player.getLevel());
    }*/


}
