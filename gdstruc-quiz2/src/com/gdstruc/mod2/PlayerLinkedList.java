package com.gdstruc.mod2;

public class PlayerLinkedList {
    //first player should be the head, succeeding elements will have a reference
    private PlayerNode head;
    private PlayerNode prev = null;

    //add element add to front
    public void addToFront(Player player)
    {
        //connect the new element to previous head
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;

        //attach prev element to upcoming head - for doubly linked list
        playerNode.setPrevPlayer(prev);
        prev = playerNode;
    }

    public void removeFront()
    {
        PlayerNode tempNode = head;
        head = head.getNextPlayer(); //sets the new head from prevhead's next
        tempNode = null; //lose the address forever/ perma delete

    }

    //similar to contains() of array list
    public boolean contains(Player player)
    {
        boolean isHere = false;
        PlayerNode current = head;
        System.out.println("Looking for: "+ player);
        while (current != null){
            if (current.getPlayer() == player){
                isHere = true;
                break;
            }
            else { current = current.getNextPlayer();}
        }
        return isHere;
    }

    public int indexOf(Player player)
    {
        int inIndex = 0,
            count = 0;
        PlayerNode current = head;
        System.out.println("What index is: "+ player);
        while (current != null){
            if (current.getPlayer() == player){
                inIndex = count;
                break;
            }
            else {
                current = current.getNextPlayer();
                count++;
                inIndex = -1;
            }
        }
        return inIndex;
    }

    //function tht will print inside linkedlist, know when to stop (if at null)
    //will not output the contents but the addresses w/o making a toString of its own
    public void printList() {
        int elementCount = 0;
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
            elementCount++;
        }
        System.out.println("null");
        System.out.println("Element count: " + elementCount);
    }

}
