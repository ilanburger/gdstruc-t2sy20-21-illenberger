package com.gdstruc.midterms;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //for reading user input
        int cardAmt, roundCount = 1;
        String command;

        //prepare the decks
        CardStack dealerDeck = new CardStack(); //dealerdeck is the pile, NOT PLAYER HAND
	    populateDealerDeck(dealerDeck);
	    CardStack discardPile = new CardStack();
	    CardStack playerHand = new CardStack();

	    //insert bot rng command issuer
        System.out.println("There is a deck of cards on the table. The dealer will issue one of the following commands.");
        System.out.println("1. Draw cards from deck \n2. Discard from your hand \n3. Retrieve from discarded pile.\n");

        while (dealerDeck.checkCardAmount() != 0) {
            System.out.println("==================================");
            System.out.println("ROUND " + roundCount);
            command = issueCommand();
            System.out.println("The chosen command is..." + command);

            //call function of command
            switch (command) {
                case "DRAW":
                    drawCards(playerHand, dealerDeck);
                    break;
                case "DISCARD":
                    if (playerHand.checkCardAmount() != 0) {
                        discardCards(playerHand, discardPile);
                    } else {
                        System.out.println("Command cannot be issued, no cards in your hand.");
                    }
                    break;
                case "RETRIEVE":
                    if (discardPile.checkCardAmount() != 0) {
                        retrieveCards(playerHand, discardPile);
                    } else {
                        System.out.println("Command cannot be issued, no cards in the discard pile.");
                    }
                    break;
            }
            printDeckLists(playerHand, dealerDeck, discardPile);
            System.out.println("==================================");
            roundCount++;
        }
        System.out.println("The deck has been emptied.");
    }

    public static CardStack populateDealerDeck(CardStack dealerDeck)
    {
        String cardNames [] = {"Twilight", "Dawn", "Dusk", "Day", "Sunset", "Sunrise", "Noon", "Evening", "Midnight"};
        int nameIndex;
        Random r = new Random();
        Card populate = null;
        for (int i = 0; i < 30; i++){
            //don't stop making cards until it becomes 30
            nameIndex = r.nextInt(9);
            populate =  new Card(cardNames[nameIndex]);
            dealerDeck.push(populate);
        }
        return dealerDeck;
    }

    public static String issueCommand()
    {
        String commands[] = {"DRAW", "DISCARD", "RETRIEVE"};
        Random r = new Random();
        int i = r.nextInt(commands.length); //randomize index
        return commands[i];
    }

    public static int cardAmount(int max)
    {
        Random r = new Random();
        int amount = 1 + r.nextInt(max);
        System.out.println("cards: +" + amount);
        return amount;
    }

    public static void drawCards(CardStack playerHand, CardStack dealerDeck)
    {
        System.out.print("\nDealer is drawing...\nGain ");
        int amount;
        if(dealerDeck.checkCardAmount() < 5){
            amount = cardAmount(dealerDeck.checkCardAmount());
        }
        else{
            amount = cardAmount(5);
        }

        //temporarily hold the cards pulled from dealer then store into player
        Card hold = null;
        //CardStack temp = new CardStack();
        for (int i = 0; i < amount; i++) {
            hold = dealerDeck.peek();
            playerHand.push(hold);
            dealerDeck.pop();
        }
    }

    public static void discardCards(CardStack playerHand, CardStack discardPile)
    {
        System.out.print("\nDiscarding cards into discard pile...\nDiscarded ");
        int amount = cardAmount(playerHand.checkCardAmount()); //check if hand EVEN HAS ENOUGH, range will be limited accordingly

        Card hold = null;
        for (int i = 0; i < amount; i++) {
            hold = playerHand.peek();
            discardPile.push(hold);
            playerHand.pop();
        }
    }

    public static void retrieveCards(CardStack playerHand, CardStack discardPile)
    {
        System.out.print("\nDealer is retrieving from discard pile...\nGain ");
        int amount = cardAmount(discardPile.checkCardAmount()); //check if pile EVEN HAS ENOUGH, range will be limited accordingly

        Card hold = null;
        //need to double check if pile EVEN HAS ENOUGH
        for (int i = 0; i < amount; i++) {
            hold = discardPile.peek();
            playerHand.push(hold);
            discardPile.pop();
        }
    }

    public static void printDeckLists(CardStack playerHand, CardStack dealerDeck, CardStack discardPile)
    {
        //print player's cards on hand & total
        System.out.println("\nThe cards in your hand right now are...");
        playerHand.printDeck();

        //print playerdeck and discarded pile amount of cards
        System.out.println("There are " + dealerDeck.checkCardAmount() + " cards remaining in the deck.");
        System.out.println("There are " + discardPile.checkCardAmount() + " cards remaining in the discarded pile.");
    }

}
