package com.gdstruc.quiz6;

public class Tree {
    private  Node root;

    public  void  insert (int value){
        if (root==null){
            root = new Node(value);
        }
        else{
            root.insert(value);
        }
    }

    public void traverseInOrder()
	{
		if (root != null){
		    root.traverseInOrder();
        }
	}

	public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    //QUIZ 06
    public Node getMin()
    {
        if (root != null)
        {
            return root.getMin();
        }
        return  null;
    }

     public Node getMax()
    {
        int minNum = 0;
        if (root != null)
        {
            return root.getMax();
        }
        return  null;
    }

    public void traverseInDescOrder()
	{
		if (root != null){
		    root.traverseInDescOrder();
        }
	}
}
