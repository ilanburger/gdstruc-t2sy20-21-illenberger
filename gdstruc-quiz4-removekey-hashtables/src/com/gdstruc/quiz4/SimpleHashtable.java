package com.gdstruc.quiz4;

public class SimpleHashtable {
    //private  Player[] hashtable; //old, doesnt determine where key is of collided element
    private  StoredPlayer[] hashtable;

    public SimpleHashtable()
    {

        //hashtable = new Player[10]; //old
        hashtable = new StoredPlayer[10]; //in the beginning only
    }

    private int hashKey(String key)
    {
        //to make sure that we are mapping the key into a range our hashtable can handle
        //there are other methods of hashing ur keys
        return key.length() % hashtable.length;
    }

    public void  put(String key, Player value)
    {
        //not the best hash function
        int hashedKey = hashKey(key);
        //after that, check if theres already a value inside the specific hashkey we've retrieved
        //will generate collisions, collisions = try to hash 2 diff values, ends up in same key
        //check if theres a value in a certain hashkey that we r going to generate

        if (isOccupied(hashedKey)){
            //do the linear probing
            //set stopping index
            int stoppingIndex = hashedKey; //where we detect the collision;

            if (hashedKey == hashtable.length -1) {
                hashedKey = 0; //wrap it back up if the others is still full and u reach the end of array
            }
            else {
                hashedKey++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex){ //means wrapped around our array
                hashedKey = (hashedKey+1) % hashtable.length;
            }
        }

        /*if (hashtable[hashedKey] != null) {
            System.out.println("There's already an element at the position " + hashedKey);
        }*/ //part 1 of vid

        if (isOccupied(hashedKey)) {
            System.out.println("There's already an element at the position " + hashedKey);
        } //still possible to be rehash our key, 2nd check here

        else {
            //hashtable[hashedKey] = value; //old
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }

    public Player get(String key) //sharing keys will give the same first occupant w/ collision
    { //how to track our collided element is placed in, need to keep track both key & value
        /*int hashedKey = hashKey(key);
        return  hashtable[hashedKey];*/ //old

        int hashedKey = findKey(key);

        if (hashedKey == -1){ //didnt find the element
            return null;
        }
        return hashtable[hashedKey].value;
    }

    public Player remove(String key){
        int hashedKey = findKey(key);

        if (hashedKey == -1){
            return null;
        }

        Player toRemove = hashtable[hashedKey].value; //stores to inform which key is being removed.
        hashtable[hashedKey].value = null; //updates the storedplayer to null

        return toRemove;
    }

    private int findKey(String key)
    {
        int hashedKey = hashKey(key);

        //determines if we find the right key
        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
            return hashedKey; //if not found then linear probe
        }

        int stoppingIndex = hashedKey; //where we detect the collision;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0; //wrap it back up if the others is still full and u reach the end of array
        } else {
            hashedKey++;
        }

        while (hashedKey != stoppingIndex
                && hashtable[hashedKey] != null
                && !hashtable[hashedKey].key.equals(key)) {
            hashedKey = (hashedKey + 1) % hashtable.length;
            //2 possibilities: found & looked into-still none
        }

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)){
            return hashedKey;
        }
        return -1;
    }



    public void  printHashtable()
    {
        for (int i=0; i < hashtable.length; i++){
            System.out.println("Element " + i + " " + hashtable[i]);
        }
    }
}
