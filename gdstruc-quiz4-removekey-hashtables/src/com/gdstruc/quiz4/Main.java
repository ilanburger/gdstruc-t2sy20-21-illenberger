package com.gdstruc.quiz4;

public class Main {

    public static void main(String[] args) {
        Player ploo = new Player(134, "Plooful", 135);
        Player wardell = new Player(536, "TSM Wardell", 640);
        Player deadlyJimmy = new Player(32, "DeadlyJimmy", 34);
        Player subroza = new Player(4931, "Subroza", 604);
        Player annieDro = new Player(6919, "09 Annie", 593);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(ploo.getUserName(), ploo);
        hashtable.put(wardell.getUserName(), wardell);
        hashtable.put(deadlyJimmy.getUserName(), deadlyJimmy);
        hashtable.put(subroza.getUserName(), subroza);
        hashtable.put(annieDro.getUserName(), annieDro);

        hashtable.printHashtable();

        //try to retrieve an element
        System.out.println("RETRIEVE: " + hashtable.get("Subroza"));
        System.out.println("REMOVE: " + hashtable.remove("09 Annie"));

        hashtable.printHashtable();

        //will retrieve the wrong (1st) element if they collided w/ a similar element if we didn't get the key

        //open addressing = when there is collision, then we'll find the next empty space,
        //put the collided element in there
        //linear probing = looking for empty space
    }
}
