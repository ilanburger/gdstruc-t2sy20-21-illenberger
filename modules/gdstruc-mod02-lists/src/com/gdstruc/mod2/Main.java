package com.gdstruc.mod2;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Player> playerList = new ArrayList<>(); //will throw an error if list package isnt imported

        //adding another instance to the list (at the end)
        playerList.add(new Player(1,"Asuna",100)); //starts at 0, ZERO
        playerList.add(new Player(2,"LethalBacon",205)); //ONE
        playerList.add(new Player(3,"HPDeskjet",34)); //TWO...

        //accessing a single element in the array list (via get())
        System.out.println(playerList.get(1)); //same as a regular array, access via the index, will take the same amt of steps O(n)

        //normally inserts a new element at the end but WHAT IF MIDDLE?
        //playerList.add(int index, Player element);
        playerList.add(2, new Player(553,"Arctis", 55)); // O(n) this method shifts all the other elements to the right

        //removing an element
        playerList.remove(2); //O(n), shifts elements again to the left

        //contains - a boolean that returns true if it has the element
        System.out.println(playerList.contains(new Player(2, "LethalBacon", 205))); //if executed now will return false
        //why? bcos its a "new" instance of the currently existing one, to fix it go back to player class & override w/ an equal() & hashcode()

        //knowing the index of a specific list/ Player; indexOf, will return -1 if list doesnt have the specified element
        System.out.println(playerList.indexOf(new Player(2, "LethalBacon", 205)));

        //playerList.forEach(player -> System.out.println(player)); //lambda method

        for (Player p: playerList) //p here is an alias
        {
            System.out.println(p); //bcos of the toSpring method, itll print out the values of our player
        }
    }
}
