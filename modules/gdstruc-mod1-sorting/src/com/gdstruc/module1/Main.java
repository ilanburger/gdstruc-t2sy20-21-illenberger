package com.gdstruc.module1;

import com.sun.jdi.event.StepEvent;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];

        numbers[0] = 23;
        numbers[1] = 245;
        numbers[2] = 5;
        numbers[3] = 78;
        numbers[4] = 28;
        numbers[5] = 12;
        numbers[6] = 56;
        numbers[7] = 67;
        numbers[8] = 89;
        numbers[9] = 356;

        ///bubbleSort(numbers);

        System.out.println("Before selection sort:");
        printArrayElements(numbers);

        selectionSort(numbers);

        System.out.println("\n\nAfter selection sort:");
        printArrayElements(numbers);
    }

    //ascending order

    private static void bubbleSort(int[] arr){
        for (int lastSortedIndex = arr.length-1; lastSortedIndex > 0; lastSortedIndex--){
            //-1 because index starts at 0, everytime u sort a value at the ends, deduct the count
            for (int i = 0; i < lastSortedIndex; i++){
                //traverse each element and compare to the next one
                if (arr [i] > arr[i+1]){
                    //if i is greater then swap values
                    int temp = arr[i]; //i has the greater value, let temp hold it
                    arr[i] = arr[i+1]; //swap i+1's value to old holder
                    arr [i+1] = temp; //store the greatest value in the next index to go to end
                }
            }
        }
    }

    //biggest value

    private static void selectionSort(int[] arr){
        for (int lastSortedIndex = arr.length -1; lastSortedIndex > 0; lastSortedIndex--){
            int largestIndex = 0; //puts at beginning
            for (int i = 1; i <= lastSortedIndex; i++){
                if (arr[i] > arr[largestIndex]){
                    largestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr){
        for (int j : arr){
            System.out.println(j);
        }
    }
}
