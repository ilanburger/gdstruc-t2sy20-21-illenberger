package com.gdstruc.mod04;

import java.util.NoSuchElementException;

public class ArrayQueue {

    //need 3 elements, array that will be backing our queue,
    //front and back
    private Player[] queue;
    private int front;
    private int back;

    public  ArrayQueue(int capacity)
    {
        queue = new Player[capacity];
    }

    //when adding a new element, always add it at the back,
    //only worry about resizing array if its full
    //in stacks, theres a TOP int = which is the TOP ELEMENT?, same with BACK int
    //always length-1, back of our queue

    public void add(Player player)
    {
        //also known as enqueue
        //resizes array when it's full
        if (back == queue.length)
        {
            Player[] newArray = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
        }

        //add element here
        queue[back] = player;
        back++;
    }

    public Player remove()
    {
        //is queue empty? need to know the size, determine size first (size();)
        if (size() == 0){
            throw new NoSuchElementException();
        }

        Player removedPlayer = queue[front];
        queue[front] = null; //nullify front element of our queue
        front++; //increase the value of front, just totrack where our first element is

        //if we remove the element, we're not shifting, we're just modifying the value of the front
        //we need to check if once we remove an element, is our queue empty? if it is, then we can reset
        //our front & back elements back to 0

        if(size()==0) //reset trackers when queue is empty
        {
            front = 0;
            back =0;
        }
        return  removedPlayer;
    }

    public int size(){
        //determine size here
        return  back-front; //front is always the 0, back is the same TOP int, points to hte last element
    }

    public Player peek()
    {
        //when peeking we are accessing the first element in front of our queue
        //need to check if our queue is empty
        if (size() == 0){
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public void printQueue()
    {
        for(int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }
}
