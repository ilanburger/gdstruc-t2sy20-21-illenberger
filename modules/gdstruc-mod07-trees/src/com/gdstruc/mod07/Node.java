package com.gdstruc.mod07;

public class Node {
    private  int data;
    private  Node rightChild;
    private  Node leftChild;

    public  void  insert(int value){ //this particular tree wudnt be able to support duplicate values

        if (value == data){ //dont accept dupes
            return;
        }

        if (value < data){ //left child
            //does it have a child yet or nah
            if (leftChild == null){
                leftChild = new Node(value);
            }
            else {
                leftChild.insert(value); //insert function is from its node class, recursion
            }
        }

        else{ //rite child
            //does it have a child yet or nah
            if (rightChild == null){
                rightChild = new Node(value);
            }
            else {
                rightChild.insert(value); //insert function is from its node class, recursion
            }
        }
    }

    //tree traversal: in-order
    public void traverseInOrder()
    {
        if(leftChild != null){
            leftChild.traverseInOrder();
        }
        System.out.println("Data:" + data);

        if(rightChild != null){
            rightChild.traverseInOrder();
        }
        }

    //retrieval function
    public Node get(int value){
        if (value==data){
            return this;
        }
        if (value < data){
            if (leftChild != null){
                return  leftChild.get(value);
            }
        }
        else {
            if (rightChild != null) {
                return rightChild.get(value);
            }
        }
        return  null; //dont find the value
    }

    public  Node(int _data){
        this.data = _data; //for every node it wud be a leaf node at first, then connect it later
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
}
