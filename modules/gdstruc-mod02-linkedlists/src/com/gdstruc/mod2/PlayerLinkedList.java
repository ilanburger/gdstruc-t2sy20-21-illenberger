package com.gdstruc.mod2;

public class PlayerLinkedList {
    //first player should be the head, succeeding elements will have a reference
    private PlayerNode head;

    //add element add to front
    public void addToFront(Player player)
    {
        //connect the new element to previous head
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    //function tht will print inside linkedlist, know when to stop (if at null)
    //will not output the contents but the addresses w/o making a toString of its own
    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }


}
