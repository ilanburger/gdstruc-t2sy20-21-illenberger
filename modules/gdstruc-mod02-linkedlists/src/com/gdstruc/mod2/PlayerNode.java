package com.gdstruc.mod2;

public class PlayerNode {
    private Player player;
    private PlayerNode nextPlayer;

    public PlayerNode(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(PlayerNode nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    //this is needed if u want to access the contents
    @Override
    public String toString() {
        return "Player{" +
                "id=" + player.getId() +
                ", name='" + player.getName() + '\'' +
                ", level=" + player.getLevel() +
                '}';
    }
}
