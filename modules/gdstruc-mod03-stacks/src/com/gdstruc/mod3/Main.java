package com.gdstruc.mod3;

public class Main {

    public static void main(String[] args) {
	// ideal backing structure for a stack is a linked list
        //stacks working with arrays
        ArrayStack stack = new ArrayStack(5);

        stack.push(new Player(1,"aceu",100));
        stack.push(new Player(2,"Sinatraa", 100));
        stack.push(new Player(3,"Subroza", 95));
        stack.push(new Player(4,"ploo", 90)); //current top element

        stack.printStack();

        System.out.println("Popping: " +  stack.pop()); //will remove the current top(ploo)
        stack.printStack();

        //stacks using linked lists

        //deque class is a more complete implementation of a stack; linkedlists is already a working stack

        LinkedStack linkstack = new LinkedStack();

        linkstack.push(new Player(1,"aceu",100));
        linkstack.push(new Player(2,"Sinatraa", 100));
        linkstack.push(new Player(3,"Subroza", 95));
        linkstack.push(new Player(4,"ploo", 90)); //current top element

        linkstack.printStack();

        System.out.println("Popping: " +  linkstack.pop()); //will remove the current top(ploo)
        linkstack.printStack();
    }
}
