package com.gdstruc.mod3;

import java.util.EmptyStackException;

public class ArrayStack {
    private Player[] stack;
    private int top; //initialized with 0

    public ArrayStack(int capacity)
    {//initialize the array capacity here
        stack = new Player[capacity];
    }

    public  void push(Player player)
    {// follows LIPO =last in first out; last element added is the top
        //check first if stack is full
        if (top == stack.length) //then resize array
        {
            Player[] newStack = new Player[2*stack.length];
            //then copy old elements to our new element
            System.arraycopy(stack, 0, newStack, 0, stack.length);
            stack = newStack;
        }
        //push new element
        stack[top++] = player; //the top element will now be the new player that we added
        // alternatively: stack[top] = player; top++;
    }

    public  Player pop()
    {
        //u might want to do smth with the player afterwards
        if (isEmpty())
        {
            throw new EmptyStackException(); //error =/= exceptions, still can be handled by code
        }

        Player poppedPlayer = stack[--top];
        stack[top] = null;
        return  poppedPlayer;
    }

    public boolean isEmpty()
    {
        return top==0;
    }

    public Player peek()
    {//when u peek, ur not removing it, u are trying to access an element
        if(isEmpty())
        {
            throw new EmptyStackException(); //cant peek an empty stack
        }

        return stack[top -1]; //the topmost value of stack should always be empty, available slot
    }

    public void printStack()
    {
        for (int i = top-1; i >= 0; i--)
        {
            System.out.println(stack[i]);
        }
    }
}
