package com.gdstruc.mod3;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedStack {
    //use linked list class
    private LinkedList<Player> stack;//this is a generic class so specify class to handle

    public LinkedStack()
    {
        stack = new LinkedList<Player>(); //dont need to initialize capacity, LLs are resizable
    }

    //these functions are already implemented by the LL class, just making L-stack class
    //as a focused version of LL that works as a stack

    public void push (Player player)
    {
        stack.push(player);
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public Player pop()
    {
        return stack.pop();
    }

    public Player peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<Player> iterator = stack.listIterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
}
