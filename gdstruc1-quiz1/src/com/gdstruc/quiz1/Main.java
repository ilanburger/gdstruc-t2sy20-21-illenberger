package com.gdstruc.quiz1;

public class Main {

    public static void main(String[] args) {
	int[] numbers = new int[10];

	numbers[0] = 23;
        numbers[1] = 245;
        numbers[2] = 5;
        numbers[3] = 78;
        numbers[4] = 28;
        numbers[5] = 12;
        numbers[6] = 56;
        numbers[7] = 67;
        numbers[8] = 89;
        numbers[9] = 356;

        System.out.println("Before sorting:");
        printArrayElements(numbers);

        //just comment out either functions as necessary
        selectionSort(numbers);
        //bubbleSort(numbers);

        System.out.println("\n\nAfter selection sorting:");
        printArrayElements(numbers);
    }

    //descending order

     private static void bubbleSort(int[] arr){
        for (int lastSortedIndex = arr.length-1; lastSortedIndex > 0; lastSortedIndex--){
            //start at the end, keep going and decrease for every comparison u make

            for (int i = 0; i < lastSortedIndex; i++){
                //traverse each element and compare to the next one

                if (arr [i] < arr[i+1]){
                    //if i is greater then swap values

                    int temp = arr[i]; //i has the lesser value, let temp hold it
                    arr[i] = arr[i+1]; //swap i+1's value to old holder
                    arr [i+1] = temp; //store the smallest value in the next index to go to front
                }
            }
        }
    }

    //smallest value

    private static void selectionSort(int[] arr){
        for (int firstSortedIndex = 0; firstSortedIndex < arr.length-1; firstSortedIndex++){
            //move up everytime u sort the biggest value in the front
            int smallestIndex = firstSortedIndex; //assign the new first as the smallest
            for (int i = firstSortedIndex+1 ; i < arr.length; i++){
                //traverse the list, +1 because one index is locked off already
                if (arr[i] > arr[smallestIndex]){
                    //comparison
                    smallestIndex = i;
                }
            }

            int temp = arr[smallestIndex];
            arr[smallestIndex] = arr[firstSortedIndex];
            arr[firstSortedIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr){
        for (int j : arr){
            System.out.println(j);
        }
    }
}
