package com.gdstruc.quiz5;

public class Main {

    public static void main(String[] args) {
	 int numbers[] = {60, 33, 12, 64, 17, 105, -53};
	 int numbers2[] = {2, 72, 34, 21, 89, 18, 12};

	 //System.out.println(linearSearch(numbers, 105));
	 //System.out.println(linearSearch(numbers, 420));

	 //System.out.println(binarySearch(numbers, 33));
	 //System.out.println(binarySearch(numbers, 420));

    //OWN SEARCH ALGORITHM, not very efficient tho me thinks
    System.out.println(everyThreesSearch(numbers2, 12));
    }

    public static int linearSearch(int[] input, int value)
    {
        for (int i = 0; i < input.length; i++)
        {
            if (input[i] == value){
             return i;
            }
        }
        return -1;
    }

     public static int binarySearch(int[] input, int value)
    {
        int start = 0;
        int end = input.length -1;

        while (start <= end)
        {
            int middle = (start + end) / 2;

            if (input[middle]==value)
            {
                return  middle;
            }
            else if (value < input[middle])
            {
                end = middle -1; //value mite be at left half
            }
            else  if (value > input[middle])
            {
                start = middle+1; //value mite be at rite half
            }
        }
        return -1;
    }

    public static int everyThreesSearch (int[] input, int value)
    {//data doesnt need to be sorted, every 3 (but we starting from 0 so..
        int copy[] = input;
        int replaceCount = 0;

        while (copy.length > 2) {
            for (int i = 2; i < copy.length; i += 2) {
                if (i % 2 == 0) {
                    if (copy[i] == value) {
                        return i + replaceCount;
                    }
                    copy = deleteElement(copy, i); //delete index then shift and replace array
                    replaceCount++;
                }
            }
        }

        if(copy.length < 3){
            for (int i=0; i < 3;i++){
                if(copy[i] == value){
                    return i;
                }
            }
        }
        return  -1;
    }

    public  static  int[] deleteElement (int[] copy, int toDel)
    {
        int newCopy[] = new int[copy.length-1];
        for (int i = 0; i < newCopy.length; i++){
            if (i < toDel) {
                newCopy[i] = copy[i];
            }
            else {
                newCopy[i] = copy[i+1]; //copies the next element
            }
        }
        return newCopy;
    }
}
